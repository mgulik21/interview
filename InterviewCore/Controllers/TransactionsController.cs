﻿using System;
using InterviewCore.Common.Model;
using InterviewCore.Common.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace InterviewCore.Controllers
{
    //TODO
    //use async await for better performance
    //add caching
    //implement HATEOAS
    //API versioning
    //unit tests
    //logging
    [Route("[controller]")]
    public class TransactionsController : Controller
    {
        private readonly ITransactionService _transactionService;

        public TransactionsController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [HttpGet]
        public IStatusCodeActionResult GetTransactions()
        {
            //TODO
            //Authentication
            //Authorization
            return Ok(_transactionService.GetTransactions());
        }

        [HttpGet("{id}")]
        public IStatusCodeActionResult GetTransactionById(Guid id)
        {
            //TODO
            //Authentication
            //Authorization

            var transaction = _transactionService.GetTransactionById(id);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        [HttpDelete("{id}")]
        public IStatusCodeActionResult DeleteTransactionById(Guid id)
        {
            //TODO
            //Authentication
            //Authorization

            _transactionService.DeleteTransaction(id);

            return Ok();
        }

        [HttpPost]
        public IStatusCodeActionResult CreateTransaction([FromBody] Transaction transaction)
        {
            //TODO
            //Authentication
            //Authorization
            //Handle case when transaction with specified id exists
            
            _transactionService.CreateTransaction(transaction);

            return Ok();
            //return Created();
        }

        [HttpPut("{id}")]
        public IStatusCodeActionResult UpdateTransaction(Guid id, [FromBody] Transaction transaction)
        {
            //TODO
            //Authentication
            //Authorization

            var currenTransaction = _transactionService.GetTransactionById(id);

            if (currenTransaction == null)
            {
                return NotFound();
            }

            _transactionService.UpdateTransaction(transaction);

            return Ok();
        }
    }
}
