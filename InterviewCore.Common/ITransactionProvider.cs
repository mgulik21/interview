﻿using System.Collections.Generic;
using InterviewCore.Common.Model;

namespace InterviewCore.Common
{
    public interface ITransactionProvider
    {
        IReadOnlyCollection<Transaction> GetTransactions();
    }
}
