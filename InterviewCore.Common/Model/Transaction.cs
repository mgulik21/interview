﻿using System;

namespace InterviewCore.Common.Model
{
    public class Transaction
    {
        public Guid Id { get; set; }
        public Int64 ApplicationId { get; set; }
        public string Type { get; set; }
        public string Summary { get; set; }
        public double Amount { get; set; }
        public DateTime PostingDate { get; set; }
        public bool IsCleared { get; set; }
        public DateTime? ClearedDate { get; set; }
    }
}