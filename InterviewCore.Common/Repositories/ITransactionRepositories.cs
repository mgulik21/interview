﻿using System;
using System.Collections.Generic;
using InterviewCore.Common.Model;

namespace InterviewCore.Common.Repositories
{
    public interface ITransactionRepositories
    {
        IReadOnlyCollection<Transaction> GetTransactions();
        Transaction GetTransactionById(Guid id);
        void DeleteTransaction(Guid id);
        void UpdateTransaction(Transaction transaction);
        void CreateTransaction(Transaction transaction);
    }
}
