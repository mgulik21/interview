﻿using System;
using System.Collections.Generic;
using System.Linq;
using InterviewCore.Common.Model;

namespace InterviewCore.Common.Repositories
{
    //TODO
    //use async await for better performance
    //setup some DB using 
    //logging
    public class TransactionRepositories : ITransactionRepositories
    {
        private readonly ITransactionProvider _transactionProvider;

        public TransactionRepositories(ITransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public IReadOnlyCollection<Transaction> GetTransactions()
        {
            return _transactionProvider.GetTransactions();
        }

        public Transaction GetTransactionById(Guid id)
        {
            return _transactionProvider.GetTransactions().SingleOrDefault(x => x.Id == id);
        }

        public void DeleteTransaction(Guid id)
        {
            throw new NotImplementedException();
        }

        public void UpdateTransaction(Transaction transaction)
        {
            throw new NotImplementedException();
        }

        public void CreateTransaction(Transaction transaction)
        {
            throw new NotImplementedException();
        }
    }
}