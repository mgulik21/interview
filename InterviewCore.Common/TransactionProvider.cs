﻿using System;
using System.Collections.Generic;
using System.IO;
using InterviewCore.Common.Model;
using Newtonsoft.Json;

namespace InterviewCore.Common
{
    //TODO
    //use async await for better performance
    //logging
    public class TransactionProvider : ITransactionProvider
    {
        private Lazy<IReadOnlyCollection<Transaction>> _transactionsLazy;

        public TransactionProvider()
        {
            _transactionsLazy = new Lazy<IReadOnlyCollection<Transaction>>(LoadTransactions);
        }

        public IReadOnlyCollection<Transaction> GetTransactions()
        {
            return _transactionsLazy.Value;
        }

        private IReadOnlyCollection<Transaction> LoadTransactions()
        {
            using (var reader = new StreamReader(@"..\data.json"))
            {
                var json = reader.ReadToEnd();
                var transactions = JsonConvert.DeserializeObject<Transaction[]>(json);

                return transactions;
            }
        }
    }
}