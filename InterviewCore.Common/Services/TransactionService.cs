﻿using System;
using System.Collections.Generic;
using InterviewCore.Common.Model;
using InterviewCore.Common.Repositories;

namespace InterviewCore.Common.Services
{
    //TODO
    //use async await for better performance
    //unit tests
    //logging
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepositories _transactionRepositories;

        public TransactionService(ITransactionRepositories transactionRepositories)
        {
            _transactionRepositories = transactionRepositories;
        }

        public IReadOnlyCollection<Transaction> GetTransactions()
        {
            return _transactionRepositories.GetTransactions();
        }

        public Transaction GetTransactionById(Guid id)
        {
            return _transactionRepositories.GetTransactionById(id);
        }

        public void DeleteTransaction(Guid id)
        {
            _transactionRepositories.DeleteTransaction(id);
        }

        public void UpdateTransaction(Transaction transaction)
        {
            _transactionRepositories.UpdateTransaction(transaction);
        }

        public void CreateTransaction(Transaction transaction)
        {
            _transactionRepositories.CreateTransaction(transaction);
        }
    }
}