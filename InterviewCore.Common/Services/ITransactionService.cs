﻿using System;
using System.Collections.Generic;
using InterviewCore.Common.Model;

namespace InterviewCore.Common.Services
{
    public interface ITransactionService
    {
        IReadOnlyCollection<Transaction> GetTransactions();
        Transaction GetTransactionById(Guid id);
        void DeleteTransaction(Guid id);
        void UpdateTransaction(Transaction transaction);
        void CreateTransaction(Transaction transaction);
    }
}
