﻿using System;
using System.Net;
using FluentAssertions;
using InterviewCore.Common.Model;
using InterviewCore.Common.Services;
using InterviewCore.Controllers;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace InterviewCore.Tests.Controllers
{
    [TestClass]
    public class TransactionsControllerTests
    {
        [TestMethod]
        public void GetTransactionById_ReturnsOk_WhenTransactionFound()
        {
            var transactionId = Guid.Parse("3f2b12b8-2a06-45b4-b057-45949279b4e5");
            var transactionServiceMock = new Mock<ITransactionService>();
            transactionServiceMock.Setup(x => x.GetTransactionById(transactionId))
                .Returns(new Transaction{Id = transactionId});
            var sut = new TransactionsController(transactionServiceMock.Object);

            var result = sut.GetTransactionById(transactionId);

            result.StatusCode.Should().Be(200);
        }

        [TestMethod]
        public void GetTransactionById_ReturnsNotFound_WhenTransactionNotFound()
        {
            var transactionId = Guid.Parse("3f2b12b8-2a06-45b4-b057-45949279b4e5");
            var transactionServiceMock = new Mock<ITransactionService>();
            transactionServiceMock.Setup(x => x.GetTransactionById(transactionId))
                .Returns((Transaction) null);
            var sut = new TransactionsController(transactionServiceMock.Object);

            var result = sut.GetTransactionById(transactionId);

            result.StatusCode.Should().Be(404);
        }
    }
}
